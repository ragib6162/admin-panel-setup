
@extends('backend.layouts.master')

@section('title', 'Admin Panel Setup')

@section('content')

<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
  {{-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> --}}
</div>
@if(auth::user()->role == 'ADMIN')
<!-- Content Row -->
<div class="row">

  <!-- Earnings (Monthly) Card Example -->
  <div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-primary shadow h-100 py-2">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Total Registered User</div>
            <div class="h5 mb-0 font-weight-bold text-gray-800"> {{ $stats['totalUser']}} </div>
          </div>
          <div class="col-auto">
            <i class="fas fa-user-friends fa-2x text-gray-300"></i>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Earnings (Monthly) Card Example -->
  <div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-success shadow h-100 py-2">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Registration Today</div>
            <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $stats['newUser']}}</div>
          </div>
          <div class="col-auto">
            <i class="fas fa-user-plus fa-2x text-gray-300"></i>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Earnings (Monthly) Card Example -->
  <div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-info shadow h-100 py-2">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Admin User</div>
            <div class="row no-gutters align-items-center">
              <div class="col-auto">
                <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">{{ $stats['adminUser']}}</div>
              </div>
              <!-- <div class="col">
                <div class="progress progress-sm mr-2">
                  <div class="progress-bar bg-info" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </div> -->
            </div>
          </div>
          <div class="col-auto">
            <i class="fas fa-user-tie fa-2x text-gray-300"></i>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Pending Requests Card Example -->
  <div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-warning shadow h-100 py-2">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Pending Requests</div>
            <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $stats['dectiveUser']}}</div>
          </div>
          <div class="col-auto">
            <i class="fas fa-user-cog fa-2x text-gray-300"></i>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
<!-- Pending Requests Card Example -->
<div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-warning shadow h-100 py-2">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Pending Requests Email</div>
            <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $stats['pendingEmail']}}</div>
          </div>
          <div class="col-auto">
            <i class="fa fa-envelope fa-2x text-gray-300"></i>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<!-- Content Row -->
@endif


<div class="row">
            <div class="col-xl-12 col-md-12">
                <div class="card shadow mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Search</h6>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{route('user_information')}}">
                            @csrf
                            <div class="form-row">
                                
                                <div class="col-md-3 form-group"></div>
                                <div class="col-md-3 form-group">
                                    <input type="email" class="form-control" id="email" placeholder="Email Address"
                                           name="email" >
                                </div>


                                <div class="col-md-3 form-group">
                                    <button type="submit" class="btn btn-primary float-right"><i
                                                class="fas fa-search fa-sm"></i> Search
                                    </button>

                                    <!-- @if (Auth::user()->role == 'ADMIN' && isset($users) && count($users) > 0)
                                        <a class="btn btn-primary float-left" target="_blank"
                                           href="{{url('export/'.$export)}}"><i
                                                    class="fas fa-download fa-sm"></i> Export
                                        </a>
                                    @endif -->
                                </div>


                            </div>
                        </form>
                        <hr/>

                        
                    </div>
                </div>
            </div>


        </div>
    </div>


@endsection