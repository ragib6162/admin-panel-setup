@extends('backend.layouts.master')

@section('title', ' User Details')

@section('content')
<h3 style="color:blue; text-align: center;"> User Information</h3>
@if (isset($details) && count($details) > 0)
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped" id="userListTable" width="100%"
                                       cellspacing="0">
                                    <thead style="color:red">
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>

                              
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse ($details as $user)
                                        <tr>
                                            <td>{{ $user->name }}</td>
                                            <td>{{ $user->email }}</td>

                                      

                                        </tr>
                                    @empty
                                        <p>No users</p>
                                    @endforelse


                                    </tbody>
                                </table>
                            </div>
                            
                        @endif
                        @if(isset($message))
                        <p style="color:red; text-align: center;">{{ $message }}</p>
                        @endif
                        <script>
        $(document).ready(function () {
            var table = $('#userListTable').DataTable();
        });

    </script>
@endsection