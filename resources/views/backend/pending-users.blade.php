@extends('backend.layouts.master')

@section('title', ' Pending Users')
@push('style')
<!-- Custom styles for this page -->
<link href="{{asset('ui/backend')}}/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
@endpush
@section('content')
<div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Dashboard</h1>
          @if(Session::has('sucess'))
          <div class=" alert alert-success"> 
                {{Session::get('sucess')}}
          </div>
          @endif
          @if(Session::has('fail'))
          <div class=" alert alert-danger"> 
                {{Session::get('fail')}}
          </div>
          @endif

          <!-- DataTales Example -->
          @if(isset($users) && count($users)>0)
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary"> Pending Users</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead style="color: red">
                    <tr>
                      <th>Name</th>
                      <th>Email</th>
                      <th>Action</th>

                    </tr>
                  </thead>
                @foreach($users as $user)
                  <tbody>
                    
                    <tr>
                      <td>{{ $user->name }}</td>
                      <td>{{ $user->email }}</td>
                      <td>
                       <form action="{{ url('/registration/approve') }}" method="post"
                       onsubmit="return confirm('Do you really want to approve?');">
                       @csrf
                       <input type="hidden" name="id" value="{{$user->id}}">
                       <button class="btn btn-sm btn-success">
                       <i class="fas fa-check fa-sm"></i>Approve
                       </button>
                       </form>
                      </td>
                      
                    </tr>
                  </tbody>
                  @endforeach
                </table>
              </div>
            </div>
          </div>
          @endif

        </div>

@push('script')
<!-- Page level plugins -->
<script src="{{asset('ui/backend')}}/vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="{{asset('ui/backend')}}/vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="{{asset('ui/backend')}}/js/demo/datatables-demo.js"></script>

@endpush

@endsection