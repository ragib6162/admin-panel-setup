<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/pending_users', function () {
//     return view('backend.pending-users');
// });

Auth::routes(['verify' => true]);
Route::group(['middleware' =>['auth', 'verified', 'approved_user']], function()
{
    Route::get('/',[App\Http\Controllers\HomeController::class, 'index'])->name('welcome');
    Route::get('/',[App\Http\Controllers\HomeController::class, 'postDashboardStats']);
    Route::post('/user_information',[App\Http\Controllers\HomeController::class, 'getUserList'])->name('user_information');
    // Route::post('/dashboard',[App\Http\Controllers\HomeController::class, 'postDashboardStats']);
    Route::get('/pending_user', [App\Http\Controllers\HomeController::class, 'getPendingUser'])->name('pending_user');
    Route::post('/registration/approve',[App\Http\Controllers\HomeController::class, 'approveUser']);

//pay order route
    Route::get('pay',[App\Http\Controllers\PayOrderController::class, 'store']);

});

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
