<?php
namespace App\Billing;


interface PaymentGatewayContract
{
    public function setDisccount($amount);
    public function charge($amount);
}

