<?php
namespace App\Billing;

use Illuminate\Support\Str;
use App\Billing\PaymentGatewayContract;

class BankPaymentGateway implements PaymentGatewayContract
{
    private $currency;
    private $discount;
    public function __construct($currency)
    {
       $this->currency = $currency;
       $this->discount = 0;
    }

    public function setDisccount($amount)
    {
       $this->discount = $amount;
    }

    public function charge($amount)
    {
     // charge the bank
     return [
         'amount' => $amount - $this->discount,
         'confirm_number' => Str::random(),
         'currency' => $this->currency,
         'discount' => $this->discount
     ];
    }
}

