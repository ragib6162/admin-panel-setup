<?php

namespace App\Http\Controllers;

use Exception;
use Carbon\Carbon;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('welcome');
    }

    public function postDashboardStats(Request $request)
    {
        // $params = $request->all();
        // $users = $this->getUserList($params, true);
        $stats = $this->getDashboardStats();
        return view('welcome', compact('stats'));
    }

    public function getUserList(Request $request)
    {
         $params = $request->all();
        //  $email = implode("",$params);
         foreach($params as $email){
        //  dd($email);
        if($email != ""){

        $user = User::where('email', 'LIKE', '%' . $email . '%')->get();
        if(count($user) > 0){
            // dd($user);
            return view('backend.user-details')->withDetails($user);
        }
    }
 }
 $message = ' no users found! ';
     return view('backend.user-details', compact('message'));  
    // dd('something'); 
    }

    private function getDashboardStats()
    {
        if (auth::user()->role != 'ADMIN'){
            return [];
        }
        $data['totalUser'] = user::count();
        $data['newUser'] = user::where('created_at', carbon::today())->count();
        $data['activeUser'] = user::where('status',1)->count();  
        $data['dectiveUser'] = user::where('status',0)->count();
        $data['adminUser'] = user::where('role','ADMIN')->count();  
        $data['pendingEmail'] = user::where('email_verified_at',NULL)->count();  

        return $data;

    }
    public function getPendingUser()
    {
        $users = User::where('status', 0)->get();
        // dd($users);
        return view('backend.pending-users', compact('users'));
    }
    public function approveUser(Request $request)
    {
        $params = $request->all();
        // dd($params);
        try{
            $user = User::where('id', $params['id'])->where('status',0)->firstOrFail();
            $user->status = 1;
            $user->save();
            return Redirect::to("/pending_user")->withSucess('User registration approved successfully.');
        }catch(ModelNotFoundException $exception){
            return Redirect::to("/pending_user")->withFail('Invalid user.');
        }catch (\Exception $exception) {
            return Redirect::to("/pending_user")->withFail('Unable to update user.');
        }
        

    }
}
